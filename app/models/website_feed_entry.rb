class WebsiteFeedEntry

  attr_accessor :id, :url, :title, :summary, :author, :content

  def initialize(attribute)
    self.id = attribute['id']
    self.url = attribute['url']
    self.title = attribute['title']
    self.author = attribute['author']
    self.summary = attribute['summary']
    self.content = attribute['content']
  end

  def self.get_all_entries(website)
      return [] unless website.present?
      response = WebsiteFeedEntryService.get_website_feed_by_params({"website_id" => website.id.to_s})
      if response.code == 200
        body = response.body
        body_arr = JSON.parse(body)
        self.init_object(body_arr)
        @collections
      end
  end

  def self.get_website_feed_entry(id)
    return [] unless id.present?
    response = WebsiteFeedEntryService.get_website_feed_by_id(id)
    if response.code == 200
      body = response.body
      body_arr = JSON.parse(body)
      new body_arr
    end
  end

  protected

  def self.init_object(json_body)
    @collections = []
    if json_body['objects'].present?
      json_body['objects'].each do |attribute|
        @collections.push(new attribute)
      end
    end
  end

end