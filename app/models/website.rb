class Website

  attr_accessor :id, :name, :domain, :subdomain, :status, :website_feed_url

  def initialize(attribute)
    self.id = attribute['id']
    self.domain = attribute['domain']
    self.subdomain = attribute['subdomain']
    self.status = attribute['status']
    self.website_feed_url = attribute['website_feed_url']
  end

  def self.get_website_by_params(params = {})
    response = WebsiteService.get_website_by_params(params)
    if response.code == 200
      body = response.body
      json_body = JSON.parse(body)
      self.init_object(json_body)
      @collections
    end
  end

  protected

  def self.init_object(json_body)
    @collections = []
    if json_body['objects'].present?
      json_body['objects'].each do |attribute|
        @collections.push(new attribute)
      end
    end
  end


end