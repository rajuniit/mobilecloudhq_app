
class WebsiteFeedEntryService

  def self.get_website_feed_by_params(params = {})
    response = Typhoeus::Request.get("#{App_Config.api_end_point}/website_feed_entries", :params => params)
  end

  def self.get_website_feed_by_id(id = 0)
    response = Typhoeus::Request.get("#{App_Config.api_end_point}/website_feed_entries/#{id}/")
  end

end