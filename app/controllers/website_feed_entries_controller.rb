class WebsiteFeedEntriesController < ApplicationController
  layout 'default'
  before_filter :check_website
  def index
    @website_feed_entries = WebsiteFeedEntry.get_all_entries(current_website)
    respond_to do |format|
      format.html
      format.json { render json: @website_feed_entries }
    end
  end

  def show
    @feed = WebsiteFeedEntry.get_website_feed_entry(params[:id])
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @feed }
    end
  end

  protected

  def check_website
    redirect_to dashboard_index_url unless current_website
  end
end