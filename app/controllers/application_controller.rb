class ApplicationController < ActionController::Base
  protect_from_forgery

  include Subdomains

  before_filter :find_website

  layout :set_layout
  protected

  def set_layout
     if env && env["HTTP_X_PJAX"].present? && !params[:_refresh]
       nil
     elsif devise_controller? || (action_name == "new" && controller_name == "users")
       'sessions'
     elsif current_website['current_theme'].present?
       current_website['current_theme']
     else
       'application'
     end
  end

  def current_website
    @current_website
  end
  def find_website
    return if App_Config.app_domain == request.host
    @current_website ||= begin
      subdomains = request.subdomains
      subdomains.delete("www") if request.host == "www.#{App_Config.app_domain}"
      website_document = Website.get_website_by_params({"domain" => request.host, "status" => "active"})
      unless website_document
        if subdomain = subdomains.first
          website_document = Website.get_website_by_params({"status" => "active", "subdomain" => subdomain})
          unless website_document.nil?
            redirect_to domain_url(:custom => website_document.first.domain)
            return
          end
        end
      end
      website_document.first
    end
    @current_website

    unless website_document.present?
      flash.now[:notice] = "Website not found"
      redirect_to domain_url(:custom => App_Config.app_domain)
      return
    end
  end

end
