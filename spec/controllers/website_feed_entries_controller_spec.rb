require 'spec_helper'

describe WebsiteFeedEntriesController do
  before(:each) do
    @website = stub_website
  end

  describe "GET index" do
    before do
      @collections = []
      website_feed_data = {}
      website_feed_data['id'] = '502d06f85f097d128a000007'
      website_feed_data['url'] = 'http://example.com'
      website_feed_data['title'] = 'Hello World'
      website_feed_data['author'] = 'raju'
      website_feed_data['summary'] = 'Hello world'
      website_feed_data['content'] = 'Hello world'
      website_feed = WebsiteFeedEntry.new(website_feed_data)
      @collections.push(website_feed)

      WebsiteFeedEntry.should_receive(:get_all_entries).and_return(@collections)
    end
    it "listed all website feed" do
      get :index
      response.should be_success
      assigns[:website_feed_entries].first.id.should == @collections.first.id
    end
  end

  describe "GET 'show'" do

    before do

      website_feed_data = {}
      website_feed_data['id'] = '502d06f85f097d128a000007'
      website_feed_data['url'] = 'http://example.com'
      website_feed_data['title'] = 'Hello World'
      website_feed_data['author'] = 'raju'
      website_feed_data['summary'] = 'Hello world'
      website_feed_data['content'] = 'Hello world'
      @website_feed = WebsiteFeedEntry.new(website_feed_data)

      WebsiteFeedEntry.should_receive(:get_website_feed_entry).and_return(@website_feed)

    end

    it "should be successful" do
      get 'show', :id => @website_feed.id
      response.should be_success
      assigns[:feed].id.should == @website_feed.id
    end
  end
end