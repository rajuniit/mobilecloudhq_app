require "spec_helper"

describe WebsiteService do
  describe "get website details using params", :vcr do
    before do
      @response = WebsiteService.get_website_by_params({:subdomain => 'raju'})
    end

    it "should return response code 200" do
      @response.code == 200
    end
  end
end
