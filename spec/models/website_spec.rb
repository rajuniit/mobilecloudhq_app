require 'spec_helper'

describe Website do

  describe "get website details using params" do
    before do
      websites = {
          :body => {"type" => "List",
                            "total" => 1,
                            "per_page" => 10,
                            "objects" => [
                                {"id"=>"502d06875f097d1052000004",
                                 "name" => "Personal website",
                                 "type" => "Website",
                                "subdomain" => "raju",
                                "domain" => "raju.mobilecloudhq.com"}
                            ]}.to_json,
          :code => 200
      }
      hash_object = Hashit.new(websites)
      WebsiteService.should_receive(:get_website_by_params).and_return(hash_object)
      response = Website.get_website_by_params
      @website = response.first
    end

    it "it should have a value of raju of key subdomain" do
       @website.subdomain == 'raju'
    end
  end


end