require 'spec_helper'

describe WebsiteFeedEntry do

  describe "website feed listing" do
    before do

      website_data = {}
      website_data['id'] = '502d06875f097d1052000004'
      website_data['domain'] = 'raju.localhost.lan'
      website_data['subdomain'] = 'raju'
      website_data['status'] = 'active'
      website_data['website_feed_url'] = 'http://www.example.com'
      website = Website.new(website_data)

      website_feed_entries = {
          :body => {"type" => "List",
                    "total" => 1,
                    "per_page" => 10,
                    "objects" => [
                        {"id"=>"502d06f85f097d128a000007",
                         "url" => "http://www.example.com/",
                         "author" => "raju",
                         "title" => "Hello World",
                         "summary" => "This is my first blog post.",
                         "content" => "This is my first blog post to help you.",}
                    ]}.to_json,
          :code => 200
      }
      hash_object = Hashit.new(website_feed_entries)
      WebsiteFeedEntryService.should_receive(:get_website_feed_by_params).and_return(hash_object)
      response = WebsiteFeedEntry.get_all_entries(website)
      @website_feed_data = response.first
    end

    it "it should have a value of Hello World of key title" do
      @website_feed_data.title == 'Hello World'
    end
  end

  describe "show a website feed details" do
      before do

        website_feed_entry = {
            :body => {
                id: "502d06f85f097d128a000007",
                url: "http://www.rajumazumder.com/",
                title: "ruby is cool",
                author: 'raju',
                summary: "Rails is rock",
                content: "How do you rest api client application",
            }.to_json,
            :code => 200
        }
        hash_object = Hashit.new(website_feed_entry)
        WebsiteFeedEntryService.should_receive(:get_website_feed_by_id).and_return(hash_object)
        response = WebsiteFeedEntry.get_website_feed_entry('502d06f85f097d128a000007')
        @website_feed_data = response
      end

      it "should have a title 'ruby is cool'" do
         @website_feed_data.title == 'ruby is cool'
      end
  end


end