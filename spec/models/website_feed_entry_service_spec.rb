require "spec_helper"

describe WebsiteFeedEntryService do
  describe "website feed listing", :vcr do
    before do
      @response = WebsiteFeedEntryService.get_website_feed_by_params({:website_id => '502d06f85f097d128a000007'})
    end

    it "should return response code 200" do
      @response.code == 200
    end
  end

  describe "show a website feed details", :vcr do
    before do
      @response = WebsiteFeedEntryService.get_website_feed_by_id("502d06f85f097d128a000007")
    end

    it "should return response code 200" do
      @response.code == 200
    end
  end
end
