module ControllerStubHelper
  protected

  def stub_website(website = nil)
    website = website || create_website
    @controller.stub!(:find_website)
    @controller.stub!(:current_website).and_return(website)
    website
  end

  def create_website
    website_data = {}
    website_data['id'] = '502d06875f097d1052000004'
    website_data['domain'] = 'raju.localhost.lan'
    website_data['subdomain'] = 'raju'
    website_data['status'] = 'active'
    website_data['website_feed_url'] = 'http://www.example.com'
    website = Website.new(website_data)
  end

end