class Hashit
  def initialize(hash)
    hash.each do |key, val|
      self.instance_variable_set("@#{key}", val)
      self.class.send(:define_method, key, proc{ self.instance_variable_get("@#{key}") })
    end
  end
end