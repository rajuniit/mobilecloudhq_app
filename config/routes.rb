MobilecloudhqCore::Application.routes.draw do

  resources :websites
  resources :dashboard
  resources :website_feed_entries

  root :to => "website_feed_entries#index"

end
