VCR.configure do |c|
  c.allow_http_connections_when_no_cassette = true
  c.cassette_library_dir = Rails.root.join("features", "vcr")
  c.hook_into :typhoeus
end

VCR.cucumber_tags do |t|
  t.tag '@vcr', use_scenario_name: true
end