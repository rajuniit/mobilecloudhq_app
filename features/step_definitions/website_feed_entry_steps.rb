When /^I visit home page as a host "([^"]*)"$/ do |host_name|
  Capybara.current_driver = :selenium
  Capybara.default_host = host_name
  Capybara.app_host = "http://#{host_name}"
  visit '/'
end