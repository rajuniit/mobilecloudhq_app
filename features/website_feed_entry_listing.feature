Feature: Website feed entry listing
  In order to see blog post details of my website
  As a user
  I want to get all posts at my home page

  @vcr
  Scenario: Get a list of all feed entry of a website
    When I visit home page as a host "raju.localhost.lan:3000"
    Then I should see "RESTful API for Orange HRM RESTful API for Orange HRM"
  @vcr
  Scenario: Redirect to main site if subdomain not found
    When I visit home page as a host "phpfour.localhost.lan:3000"
    Then I should see "Welcome To Mobile Cloud HQ"
